package su.amodule.mvpapplication;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import su.amodule.mvpapplication.mvp.presenters.AuthPresenter;
import su.amodule.mvpapplication.mvp.presenters.IAuthPresenter;
import su.amodule.mvpapplication.mvp.views.IAuthView;
import su.amodule.mvpapplication.ui.custom_views.AuthPanel;

public class RootActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {

    AuthPresenter mAuthPresenter = AuthPresenter.getInstance();

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;

    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    //region ================================= Life cycle ==================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpapplication);

        ButterKnife.bind(this);
        mAuthPresenter.takeView(this);
        mAuthPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        mAuthPresenter.dropView();
        super.onDestroy();
    }

    //endregion============================================================================

    //region ======================== IAuthView  ==================================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Smth going wrong..");
            // TODO: 22.10.2016 send error stacktrace to cradhlytics
        }
    }

    @Override
    public void showLoad() {
        // TODO: 22.10.2016 shgow load progress
    }

    @Override
    public void hideLoad() {
        // TODO: 22.10.2016 hide load progress
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mAuthPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    /*@Override
    public void testShowLoginCard() {
        mAuthCard.setVisibility(View.VISIBLE);
    }*/


    //endregion


    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                mAuthPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mAuthPresenter.clickOnLogin();
                break;
        }
    }
}
