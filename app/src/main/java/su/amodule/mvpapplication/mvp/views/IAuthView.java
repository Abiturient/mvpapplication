package su.amodule.mvpapplication.mvp.views;

import android.support.annotation.Nullable;

import su.amodule.mvpapplication.mvp.presenters.IAuthPresenter;
import su.amodule.mvpapplication.ui.custom_views.AuthPanel;

/**
 * Created by Alex on 22.10.2016.
 */
public interface IAuthView {

    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    //void testShowLoginCard();

    @Nullable
    AuthPanel getAuthPanel();
}
